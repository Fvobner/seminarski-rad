let handleLogin = function (options) {
    if (options.login) {
        
        let form = options.login.form;
        let modal = options.login.modal;

        $(form).on('submit', function (e) {
            e.preventDefault();

            let data = $(this).serializeArray();
            let dataObj = {};

            $.each(data, function(index, value){
                dataObj[value.name] = value.value === "" ? null : value.value;
            });

            $.ajax({
                async: true,
                crossDomain: true,
                url: '',
                method: 'POST', 
                contentType: 'application/json',
                data: JSON.stringify(dataObj),
                beforeSend: function(){
                    $('.modal-loader').show();
                },
                success: function(data){
                    
                },
                error: function(xhr){
                    console.log(xhr);
                },
                complete: function(){
                    setTimeout(function(){
                        $('.modal-loader').hide();
                    }, 5000);
                    
                }
            });

        });
        $(modal).on('hidden.bs.modal', function (e) {
            $(form)[0].reset();
            $(form).parsley().reset();
          })
    }
};

