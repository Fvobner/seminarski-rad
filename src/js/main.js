let App = function(){
    "use strict";

    return {
        init: function(options){
            this.initBase(options);
            this.initComponents(options);
            this.initPages(options);
        },
        initBase: function(options){
            handleLogin(options);
        },
    }
}();